/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.mycompany.llantera1;

import java.awt.Color;
import java.awt.Container;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

/**

 *
 * @author Adria
 */
public class IniciarSesion extends javax.swing.JFrame {
 Container contenedor = this.getContentPane(); // Asociando al contenedor Jframe 
    
    /**
     * Creates new form IngresarUsuario
     */
    public IniciarSesion() {
        initComponents();
        
      
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnLimpiar = new javax.swing.JButton();
        lblTitulo = new javax.swing.JLabel();
        txtContraseña = new javax.swing.JTextField();
        txtUsuario = new javax.swing.JTextField();
        lblRegistrarseN = new javax.swing.JLabel();
        lblUsuario = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        btnIniciarSesion = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        lblImagen = new javax.swing.JLabel();
        lblContraseña = new javax.swing.JLabel();
        btnSalir = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Inicio de Sesión");
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnLimpiar.setIcon(new javax.swing.ImageIcon("C:\\Users\\Adria\\OneDrive\\Documentos\\NetBeansProjects\\Llantera1\\src\\main\\java\\com\\mycompany\\llantera1\\imagenes\\limpiar.jpg")); // NOI18N
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btnLimpiar, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 270, 40, 40));
        btnLimpiar.getAccessibleContext().setAccessibleDescription("");

        lblTitulo.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        lblTitulo.setForeground(new java.awt.Color(255, 255, 255));
        lblTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitulo.setText("Inicio de Sesión ");
        getContentPane().add(lblTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 60, 460, 70));

        txtContraseña.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtContraseña.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtContraseña.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtContraseñaActionPerformed(evt);
            }
        });
        getContentPane().add(txtContraseña, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 280, 280, 30));

        txtUsuario.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtUsuario.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        getContentPane().add(txtUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 190, 280, 30));

        lblRegistrarseN.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblRegistrarseN.setForeground(new java.awt.Color(255, 255, 255));
        lblRegistrarseN.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblRegistrarseN.setText("¿No esta registrado?");
        getContentPane().add(lblRegistrarseN, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 410, 170, 20));

        lblUsuario.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblUsuario.setForeground(new java.awt.Color(255, 255, 255));
        lblUsuario.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblUsuario.setText("Usuario");
        getContentPane().add(lblUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 160, 140, 30));

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton1.setText("Registrarse");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 430, 130, 40));

        btnIniciarSesion.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnIniciarSesion.setText("Iniciar Sesión");
        btnIniciarSesion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnIniciarSesionMouseClicked(evt);
            }
        });
        btnIniciarSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIniciarSesionActionPerformed(evt);
            }
        });
        getContentPane().add(btnIniciarSesion, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 340, 130, 40));

        jLabel2.setIcon(new javax.swing.ImageIcon("C:\\Users\\Adria\\OneDrive\\Documentos\\NetBeansProjects\\Llantera1\\src\\main\\java\\com\\mycompany\\llantera1\\imagenes\\GALA.png")); // NOI18N
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 70, 350, 330));

        lblImagen.setIcon(new javax.swing.ImageIcon("C:\\Users\\Adria\\OneDrive\\Documentos\\NetBeansProjects\\Llantera1\\src\\main\\java\\com\\mycompany\\llantera1\\imagenes\\llantera5.jpg")); // NOI18N
        getContentPane().add(lblImagen, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 450, 500));

        lblContraseña.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblContraseña.setForeground(new java.awt.Color(255, 255, 255));
        lblContraseña.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblContraseña.setText("Contraseña");
        getContentPane().add(lblContraseña, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 250, 140, 30));

        btnSalir.setIcon(new javax.swing.ImageIcon("C:\\Users\\Adria\\OneDrive\\Documentos\\NetBeansProjects\\Llantera1\\src\\main\\java\\com\\mycompany\\llantera1\\imagenes\\salir.jpg")); // NOI18N
        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        getContentPane().add(btnSalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 440, -1, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon("C:\\Users\\Adria\\OneDrive\\Documentos\\NetBeansProjects\\Llantera1\\src\\main\\java\\com\\mycompany\\llantera1\\imagenes\\fondoLlanta.jpg")); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 500));

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
   
    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
      int opcion = JOptionPane.showConfirmDialog(null, "¿Seguro que quieres salir?", "Confirmar Salida", JOptionPane.YES_NO_OPTION);
    if (opcion == JOptionPane.YES_OPTION) {
        System.exit(0); // Salir de la aplicación
    }
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
     txtUsuario.setText ("");
     txtContraseña.setText ("");
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnIniciarSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIniciarSesionActionPerformed
      String url = "jdbc:mysql://localhost/llantera";
      String user = "Adrian";
      String password = "Cisco";

    try (Connection conn = DriverManager.getConnection(url, user, password)) {
     // Confirmar que la conexión fue exitosa

    String usuarioIngresado = txtUsuario.getText();
    String contraseñaIngresada = txtContraseña.getText(); // Obtener la contraseña como cadena directamente

    // Consulta SQL para obtener la contraseña almacenada en la base de datos para el usuario ingresado
    String query = "SELECT Contraseña FROM usuarios WHERE Nom_Usuario = ?";
    try (PreparedStatement preparedStatement = conn.prepareStatement(query)) {
        preparedStatement.setString(1, usuarioIngresado);
        try (ResultSet resultSet = preparedStatement.executeQuery()) {
            if (resultSet.next()) {
                String contraseñaAlmacenada = resultSet.getString("Contraseña");
                // Verificar si la contraseña ingresada coincide con la almacenada en la base de datos
                if (contraseñaIngresada.equals(contraseñaAlmacenada)) {
                    // Las credenciales son correctas, realizar el cambio de interfaz
                    TipoDeMovimientoEmpleado tipoDeMovimientoEmpleado = new TipoDeMovimientoEmpleado();
                    tipoDeMovimientoEmpleado.setVisible(true);
                    this.dispose();
                } else {
                    // Contraseña incorrecta
                    JOptionPane.showMessageDialog(null, "Contraseña incorrecta", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                // El usuario no existe
                JOptionPane.showMessageDialog(null, "Usuario no encontrado", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(this, "Disculpe hubo un error:\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        ex.printStackTrace();
    }
} catch (SQLException ex) {
    JOptionPane.showMessageDialog(this, "Disculpe hubo un error en la conexión:\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    ex.printStackTrace();
}
    }//GEN-LAST:event_btnIniciarSesionActionPerformed

    private void btnIniciarSesionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnIniciarSesionMouseClicked
        TipoDeMovimientoEmpleado tipoDeMovimientoEmpleado = new TipoDeMovimientoEmpleado();
        tipoDeMovimientoEmpleado.setVisible(true);
        this.dispose();
        
    }//GEN-LAST:event_btnIniciarSesionMouseClicked

    private void txtContraseñaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtContraseñaActionPerformed
 
    }//GEN-LAST:event_txtContraseñaActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

    }//GEN-LAST:event_jButton1ActionPerformed
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(IniciarSesion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(IniciarSesion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(IniciarSesion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(IniciarSesion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new IniciarSesion().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnIniciarSesion;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel lblContraseña;
    private javax.swing.JLabel lblImagen;
    private javax.swing.JLabel lblRegistrarseN;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JLabel lblUsuario;
    private javax.swing.JTextField txtContraseña;
    private javax.swing.JTextField txtUsuario;
    // End of variables declaration//GEN-END:variables
}
