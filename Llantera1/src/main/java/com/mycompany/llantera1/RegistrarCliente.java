/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.mycompany.llantera1;

import java.awt.Color;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Adria
 */
public class RegistrarCliente extends javax.swing.JFrame {
    public RegistrarCliente() {
        initComponents();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel8 = new javax.swing.JLabel();
        lblTitulo = new javax.swing.JLabel();
        lblUsuario = new javax.swing.JLabel();
        lblUsuario1 = new javax.swing.JLabel();
        lblUsuario2 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        lblUsuario3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        lblUsuario4 = new javax.swing.JLabel();
        btnSalir = new javax.swing.JButton();
        txtNombre = new javax.swing.JTextField();
        txtDni_c = new javax.swing.JTextField();
        txtDireccion = new javax.swing.JTextField();
        txtApellido = new javax.swing.JTextField();
        txtTelefono = new javax.swing.JTextField();
        btnLimpiar = new javax.swing.JButton();
        btnRegistrar = new javax.swing.JButton();
        btnCerrar = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();

        jLabel8.setFont(new java.awt.Font("Segoe UI", 0, 48)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 161, 255));
        jLabel8.setText("REGISTRO DE CLIENTES");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblTitulo.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        lblTitulo.setForeground(new java.awt.Color(255, 255, 255));
        lblTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitulo.setText("Registrar Clientes");
        getContentPane().add(lblTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 30, -1, -1));

        lblUsuario.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblUsuario.setForeground(new java.awt.Color(255, 255, 255));
        lblUsuario.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUsuario.setText("DNI Cliente:");
        getContentPane().add(lblUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 110, 100, -1));

        lblUsuario1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblUsuario1.setForeground(new java.awt.Color(255, 255, 255));
        lblUsuario1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUsuario1.setText("Nombre:");
        getContentPane().add(lblUsuario1, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 150, -1, -1));

        lblUsuario2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblUsuario2.setForeground(new java.awt.Color(255, 255, 255));
        lblUsuario2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUsuario2.setText("Direccion:");
        getContentPane().add(lblUsuario2, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 230, -1, -1));

        jLabel2.setIcon(new javax.swing.ImageIcon("C:\\Users\\Adria\\OneDrive\\Documentos\\NetBeansProjects\\Llantera1\\src\\main\\java\\com\\mycompany\\llantera1\\imagenes\\GALA.png")); // NOI18N
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 290, 330, 230));

        lblUsuario3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblUsuario3.setForeground(new java.awt.Color(255, 255, 255));
        lblUsuario3.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUsuario3.setText("Apellido:");
        getContentPane().add(lblUsuario3, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 190, -1, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon("C:\\Users\\Adria\\OneDrive\\Documentos\\NetBeansProjects\\Llantera1\\src\\main\\java\\com\\mycompany\\llantera1\\imagenes\\Clientes.jpg")); // NOI18N
        jLabel1.setFocusCycleRoot(true);
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, -20, -1, 590));

        lblUsuario4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblUsuario4.setForeground(new java.awt.Color(255, 255, 255));
        lblUsuario4.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUsuario4.setText("Telefono:");
        getContentPane().add(lblUsuario4, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 270, -1, -1));

        btnSalir.setIcon(new javax.swing.ImageIcon("C:\\Users\\Adria\\OneDrive\\Documentos\\NetBeansProjects\\Llantera1\\src\\main\\java\\com\\mycompany\\llantera1\\imagenes\\salir.jpg")); // NOI18N
        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        getContentPane().add(btnSalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 520, -1, -1));

        txtNombre.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtNombre.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        getContentPane().add(txtNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 150, 510, -1));

        txtDni_c.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtDni_c.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtDni_c.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDni_cActionPerformed(evt);
            }
        });
        getContentPane().add(txtDni_c, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 110, 490, -1));

        txtDireccion.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtDireccion.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        getContentPane().add(txtDireccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 230, 500, -1));

        txtApellido.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtApellido.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        getContentPane().add(txtApellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 190, 513, -1));

        txtTelefono.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtTelefono.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        getContentPane().add(txtTelefono, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 270, 500, -1));

        btnLimpiar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnLimpiar.setIcon(new javax.swing.ImageIcon("C:\\Users\\Adria\\OneDrive\\Documentos\\NetBeansProjects\\Llantera1\\src\\main\\java\\com\\mycompany\\llantera1\\imagenes\\limpiar.jpg")); // NOI18N
        btnLimpiar.setText("Limpiar");
        btnLimpiar.setMaximumSize(new java.awt.Dimension(137, 47));
        btnLimpiar.setMinimumSize(new java.awt.Dimension(137, 47));
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btnLimpiar, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 320, 160, 80));

        btnRegistrar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnRegistrar.setIcon(new javax.swing.ImageIcon("C:\\Users\\Adria\\OneDrive\\Documentos\\NetBeansProjects\\Llantera1\\src\\main\\java\\com\\mycompany\\llantera1\\imagenes\\registrar.png")); // NOI18N
        btnRegistrar.setText("Registrar");
        btnRegistrar.setMaximumSize(new java.awt.Dimension(137, 47));
        btnRegistrar.setMinimumSize(new java.awt.Dimension(137, 47));
        btnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnRegistrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 320, 160, 80));

        btnCerrar.setIcon(new javax.swing.ImageIcon("C:\\Users\\Adria\\OneDrive\\Documentos\\NetBeansProjects\\Llantera1\\src\\main\\java\\com\\mycompany\\llantera1\\imagenes\\cerrar.jpg")); // NOI18N
        btnCerrar.setText("Cerrar");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCerrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(980, 10, 110, 40));

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setIcon(new javax.swing.ImageIcon("C:\\Users\\Adria\\OneDrive\\Documentos\\NetBeansProjects\\Llantera1\\src\\main\\java\\com\\mycompany\\llantera1\\imagenes\\fondoLlanta.jpg")); // NOI18N
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1100, 590));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
          int opcion = JOptionPane.showConfirmDialog(null, "¿Seguro que quieres salir?", "Confirmar Salida", JOptionPane.YES_NO_OPTION);
        if (opcion == JOptionPane.YES_OPTION) {
            TipoDeMovimientoEmpleado tipoDeMovimientoEmpleado = new TipoDeMovimientoEmpleado();
            tipoDeMovimientoEmpleado.setVisible(true);
            this.dispose(); // Salir de la aplicación
        }
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarActionPerformed
    String url = "jdbc:mysql://localhost/llantera";
    String user = "Adrian";
    String password = "Cisco";

    String DNI_c = txtDni_c.getText();
    String nombre = txtNombre.getText();
    String apellido = txtApellido.getText();
    String dirección = txtDireccion.getText();
    String telefono = txtTelefono.getText();

    try (Connection connection = DriverManager.getConnection(url, user, password)) {
        String query = "INSERT INTO cliente (DNI_c, nombre, apellido, dirección, telefono) VALUES (?, ?, ?, ?, ?)";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, DNI_c);
        preparedStatement.setString(2, nombre);
        preparedStatement.setString(3, apellido);
        preparedStatement.setString(4, dirección);
        preparedStatement.setString(5, telefono);
        preparedStatement.executeUpdate();

        JOptionPane.showMessageDialog(this, "Cliente registrado exitosamente");

    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(this, "Error al registrar cliente:\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    }
    txtDni_c.setText("");
    txtNombre.setText("");
    txtApellido.setText("");
    txtDireccion.setText("");
    txtTelefono.setText("");
    }//GEN-LAST:event_btnRegistrarActionPerformed

    private void txtDni_cActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDni_cActionPerformed

    }//GEN-LAST:event_txtDni_cActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
    txtDni_c.setText("");
    txtNombre.setText("");
    txtApellido.setText("");
    txtDireccion.setText("");
    txtTelefono.setText("");
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        int opcion = JOptionPane.showConfirmDialog(null, "¿Seguro que quieres cerrar la aplicacion?", "Confirmar Salida", JOptionPane.YES_NO_OPTION);
        if (opcion == JOptionPane.YES_OPTION) {
            System.exit(0);
        }
    }//GEN-LAST:event_btnCerrarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RegistrarCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RegistrarCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RegistrarCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RegistrarCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RegistrarCliente().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnRegistrar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JLabel lblUsuario;
    private javax.swing.JLabel lblUsuario1;
    private javax.swing.JLabel lblUsuario2;
    private javax.swing.JLabel lblUsuario3;
    private javax.swing.JLabel lblUsuario4;
    private javax.swing.JTextField txtApellido;
    private javax.swing.JTextField txtDireccion;
    private javax.swing.JTextField txtDni_c;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtTelefono;
    // End of variables declaration//GEN-END:variables
}
