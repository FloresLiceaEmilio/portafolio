/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.mycompany.llantera1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Adria
 */
public class VistaEmpleado extends javax.swing.JFrame {

    /**
     * Creates new form VistaEmpleado
     */
    public VistaEmpleado() {
        initComponents();
     TaRes.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
        @Override
        public void valueChanged(ListSelectionEvent e) {
            if (!e.getValueIsAdjusting()) {
                int selectedRow = TaRes.getSelectedRow();
                if (selectedRow != -1) {
                    // Obtener los valores de la fila seleccionada
                    String ID_e = TaRes.getValueAt(selectedRow, 0).toString();
                    String nombre = TaRes.getValueAt(selectedRow, 1).toString();
                    String apellido = TaRes.getValueAt(selectedRow, 2).toString();
                    String cargo = TaRes.getValueAt(selectedRow, 3).toString();
                 
                    // Llenar los textfields con los valores obtenidos
                    txtID_e.setText(ID_e);
                    txtNombre.setText(nombre);
                    txtApellido.setText(apellido);
                    txtCargo.setText(cargo);
                   
                }
            }
        }
    });
 }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        TaRes = new javax.swing.JTable();
        lblTitulo = new javax.swing.JLabel();
        lblDni_e = new javax.swing.JLabel();
        lblNombre = new javax.swing.JLabel();
        lblApellido = new javax.swing.JLabel();
        lblCargo = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        txtID_e = new javax.swing.JTextField();
        txtCargo = new javax.swing.JTextField();
        txtApellido = new javax.swing.JTextField();
        btnEliminar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        btnConexion = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        btnCerrar = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        TaRes.setBackground(new java.awt.Color(153, 153, 255));
        TaRes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "ID_e", "nombre", "apellido", "Cargo"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane2.setViewportView(TaRes);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 30, 640, 400));

        lblTitulo.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        lblTitulo.setForeground(new java.awt.Color(255, 255, 255));
        lblTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitulo.setText("Vista Empleado");
        getContentPane().add(lblTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(135, 6, -1, -1));

        lblDni_e.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblDni_e.setForeground(new java.awt.Color(255, 255, 255));
        lblDni_e.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDni_e.setText("ID Empleado:");
        getContentPane().add(lblDni_e, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 110, -1));

        lblNombre.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblNombre.setForeground(new java.awt.Color(255, 255, 255));
        lblNombre.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNombre.setText("Nombre:");
        getContentPane().add(lblNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 100, -1));

        lblApellido.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblApellido.setForeground(new java.awt.Color(255, 255, 255));
        lblApellido.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblApellido.setText("Apellido:");
        getContentPane().add(lblApellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, 100, -1));

        lblCargo.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblCargo.setForeground(new java.awt.Color(255, 255, 255));
        lblCargo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblCargo.setText("Cargo:");
        getContentPane().add(lblCargo, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, 100, -1));

        txtNombre.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtNombre.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        getContentPane().add(txtNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(106, 110, 526, -1));

        txtID_e.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtID_e.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        getContentPane().add(txtID_e, new org.netbeans.lib.awtextra.AbsoluteConstraints(132, 70, 500, -1));

        txtCargo.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtCargo.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        getContentPane().add(txtCargo, new org.netbeans.lib.awtextra.AbsoluteConstraints(106, 190, 526, -1));

        txtApellido.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtApellido.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        getContentPane().add(txtApellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(106, 150, 526, -1));

        btnEliminar.setIcon(new javax.swing.ImageIcon("C:\\Users\\Adria\\OneDrive\\Documentos\\NetBeansProjects\\Llantera1\\src\\main\\java\\com\\mycompany\\llantera1\\imagenes\\Eliminar.png")); // NOI18N
        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });
        getContentPane().add(btnEliminar, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 240, 150, 80));

        btnSalir.setIcon(new javax.swing.ImageIcon("C:\\Users\\Adria\\OneDrive\\Documentos\\NetBeansProjects\\Llantera1\\src\\main\\java\\com\\mycompany\\llantera1\\imagenes\\salir.jpg")); // NOI18N
        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        getContentPane().add(btnSalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(21, 387, -1, -1));

        btnConexion.setText("Conexion");
        btnConexion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConexionActionPerformed(evt);
            }
        });
        getContentPane().add(btnConexion, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        btnModificar.setIcon(new javax.swing.ImageIcon("C:\\Users\\Adria\\OneDrive\\Documentos\\NetBeansProjects\\Llantera1\\src\\main\\java\\com\\mycompany\\llantera1\\imagenes\\Modificar.png")); // NOI18N
        btnModificar.setText("Modificar");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });
        getContentPane().add(btnModificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 240, 150, 80));

        btnCerrar.setIcon(new javax.swing.ImageIcon("C:\\Users\\Adria\\OneDrive\\Documentos\\NetBeansProjects\\Llantera1\\src\\main\\java\\com\\mycompany\\llantera1\\imagenes\\cerrar.jpg")); // NOI18N
        btnCerrar.setText("Cerrar");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCerrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 390, 110, 40));

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setIcon(new javax.swing.ImageIcon("C:\\Users\\Adria\\OneDrive\\Documentos\\NetBeansProjects\\Llantera1\\src\\main\\java\\com\\mycompany\\llantera1\\imagenes\\fondoLlanta.jpg")); // NOI18N
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1320, 450));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
      int filaSeleccionada = TaRes.getSelectedRow();
    if (filaSeleccionada >= 0) {
        int DNI_c = (int) TaRes.getValueAt(filaSeleccionada, 0);

        int confirmacion = JOptionPane.showConfirmDialog(
            this,
            "¿Estás seguro de que deseas eliminar el registro?",
            "Confirmar Eliminación",
            JOptionPane.YES_NO_OPTION
        );

        if (confirmacion == JOptionPane.YES_OPTION) {
            // Eliminar la fila de la tabla
            DefaultTableModel model = (DefaultTableModel) TaRes.getModel();
            model.removeRow(filaSeleccionada);
        String url = "jdbc:mysql://localhost:3306/llantera";
        String user = "Adrian";
        String password = "Cisco";
    
         try {
                try (Connection connection = DriverManager.getConnection(url, user, password)) {
                    String sql = "DELETE FROM empleado WHERE ID_e=?";
                    try (PreparedStatement statement = connection.prepareStatement(sql)) {
                        statement.setInt(1, DNI_c);
                        int rowsAffected = statement.executeUpdate();
                        if (rowsAffected > 0) {
                            JOptionPane.showMessageDialog(this, "El registro se eliminó correctamente.");
                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
         txtID_e.setText("");
         txtNombre.setText("");
         txtApellido.setText("");
         txtCargo.setText("");
    
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        int opcion = JOptionPane.showConfirmDialog(null, "¿Seguro que quieres salir?", "Confirmar Salida", JOptionPane.YES_NO_OPTION);
        if (opcion == JOptionPane.YES_OPTION) {
            TipoDeMovimientoEmpleado tipoDeMovimientoEmpleado = new TipoDeMovimientoEmpleado();
            tipoDeMovimientoEmpleado.setVisible(true);
            this.dispose(); // Salir de la aplicación
        }
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnConexionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConexionActionPerformed
        String url = "jdbc:mysql://localhost:3306/llantera";
        String user = "Adrian";
        String password = "Cisco";
        try {
            Connection connection = DriverManager.getConnection(url, user, password);
            String sql = "SELECT * FROM empleado";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();

            // Obtener el modelo de la tabla TaRes
            DefaultTableModel model = (DefaultTableModel) TaRes.getModel();

            // Limpiar la tabla TaRes
            model.setRowCount(0);

            // Iterar a través del resultado de la consulta y agregar los datos a la tabla TaRes
            while (resultSet.next()) {
                int ID_e = resultSet.getInt("ID_e");
                String nombre = resultSet.getString("nombre");
                String apellido = resultSet.getString("apellido");
                String Cargo = resultSet.getString("Cargo");
                

                // Agregar una nueva fila a la tabla TaRes con los datos de la consulta
                model.addRow(new Object[]{ID_e, nombre, apellido, Cargo});
            }

            // Cerrar los recursos de la base de datos
            resultSet.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnConexionActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        int selectedRow = TaRes.getSelectedRow();
        if (selectedRow != -1) {
            String ID_e = txtID_e.getText();
            String nombre = txtNombre.getText();
            String apellido = txtApellido.getText();
            String Cargo = txtCargo.getText();
         
            int confirmacion = JOptionPane.showConfirmDialog(
            this,
            "¿Estás seguro de que deseas modificar el registro?",
            "Confirmar Modificación",
            JOptionPane.YES_NO_OPTION
        );


            // Actualizar los valores en la tabla
            TaRes.setValueAt(ID_e, selectedRow, 0);
            TaRes.setValueAt(nombre, selectedRow, 1);
            TaRes.setValueAt(apellido, selectedRow, 2);
            TaRes.setValueAt(Cargo, selectedRow, 3);

            // Aquí puedes escribir el código para actualizar los datos en la base de datos
            try {
                Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/llantera", "Adrian", "Cisco");
                String updateQuery = "UPDATE empleado SET ID_e=?, nombre=?, apellido=?, Cargo=? WHERE DNI_c=?";
                PreparedStatement preparedStatement = conn.prepareStatement(updateQuery);
                preparedStatement.setString(1, ID_e);
                preparedStatement.setString(2, nombre);
                preparedStatement.setString(3, apellido);
                preparedStatement.setString(4, Cargo);
                preparedStatement.setString(6, ID_e); // Condición para identificar el registro a actualizar
                preparedStatement.executeUpdate();

                JOptionPane.showMessageDialog(null, "Registro actualizado correctamente");

                conn.close();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al actualizar el registro: " + ex.getMessage());
            }
        }
    }//GEN-LAST:event_btnModificarActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        int opcion = JOptionPane.showConfirmDialog(null, "¿Seguro que quieres cerrar la aplicacion?", "Confirmar Salida", JOptionPane.YES_NO_OPTION);
        if (opcion == JOptionPane.YES_OPTION) {
            System.exit(0);
        }
    }//GEN-LAST:event_btnCerrarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VistaEmpleado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VistaEmpleado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VistaEmpleado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VistaEmpleado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VistaEmpleado().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable TaRes;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnConexion;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblApellido;
    private javax.swing.JLabel lblCargo;
    private javax.swing.JLabel lblDni_e;
    private javax.swing.JLabel lblNombre;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JTextField txtApellido;
    private javax.swing.JTextField txtCargo;
    private javax.swing.JTextField txtID_e;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables
}
